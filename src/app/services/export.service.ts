import { Injectable } from '@angular/core';
import jsPDF from 'jspdf';
import 'jspdf-autotable';
import { Observable } from 'rxjs/internal/Observable';
@Injectable({
  providedIn: 'root'
})
export class ExportService {

  constructor() { }

  exportPdf(exportPdfColumn, fileName) {

    const doc = new jsPDF('l');

    // doc.autoTable(exportPdfColumn, exportData);
    doc.autoTable(exportPdfColumn);
    doc.save(fileName + '_' + new Date().getTime() + '.pdf');

  }


  exportExcel(exportJsonData, fileName) {
    import('xlsx').then(xlsx => {
      const worksheet = xlsx.utils.json_to_sheet(exportJsonData);
      const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
      this.saveAsExcelFile(excelBuffer, fileName);
    });
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    import('file-saver').then(FileSaver => {


      let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      let EXTENSION_TYPE = '.xlsx';
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(data, fileName + new Date().getTime() + EXTENSION_TYPE);
    });
  }


  exportCSV(exportJsonData, fileName) {
    import('xlsx').then(xlsx => {
      const worksheet = xlsx.utils.json_to_sheet(exportJsonData);
      const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = xlsx.write(workbook, { bookType: 'csv', type: 'array' });

      this.saveAsCSVFile(excelBuffer, fileName);
    });
  }

  saveAsCSVFile(buffer: any, fileName: string): void {
    import('file-saver').then(FileSaver => {


      let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      let EXTENSION_TYPE = '.csv';
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(data, fileName + new Date().getTime() + EXTENSION_TYPE);
    });
  }

  

}
