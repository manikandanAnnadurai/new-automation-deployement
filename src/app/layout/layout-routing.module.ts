import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {LayoutComponent} from './layout.component';
import { ServicecatalogComponent } from './servicecatalog/servicecatalog.component';
import { ValidationRuleComponent } from './validation-rule/validation-rule.component';
import { ClientAdminUserBoardComponent } from './client-admin-user-board/client-admin-user-board.component';
import { ClientAdministrationComponent } from './client-administration/client-administration.component';
import { UserManagementComponent } from './user-management/user-management.component';
import { UsergroupmanagementComponent } from './usergroupmanagement/usergroupmanagement.component';
import { ProjectTypeRegulationComponent } from './project-type-regulation/project-type-regulation.component';
import { ProjectdefinitionsComponent } from './projectdefinitions/projectdefinitions.component';
import { ProjectconfigurationComponent } from './projectconfiguration/projectconfiguration.component';
import { ProjectUserRoleMappingComponent } from './project-user-role-mapping/project-user-role-mapping.component';
import { RoleDefinitionComponent } from './role-definition/role-definition.component';
import { RolemanagementComponent } from './rolemanagement/rolemanagement.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { WorkflowComponent } from './workflow/workflow.component';
import { InputComponent } from './input/input.component';
//import { AuthGuard } from '../shared/guard/auth.guard';
const routes: Routes = [
    {
        path: '', 
        component: LayoutComponent,
        children: [
            { path: '', redirectTo: 'ClientAdministration'},
            // { path: 'login', loadChildren: () => import('../login/login.module').then(m => m.LoginModule) },            
            // { path: 'home', loadChildren: () => import('./home/home.module').then(m => m.homeModule)},
            { path: 'useraccesscontrol', loadChildren: () => import('./useraccesscontrol-components/useraccesscontrols.module').then(m => m.UserAccessControls)},
            { path: 'ServicecatalogComponent', component:  ServicecatalogComponent},
            { path: 'ProjectTypeRegulation', component:  ProjectTypeRegulationComponent},
            { path: 'Projectdefinitions', component:  ProjectdefinitionsComponent},
            { path: 'Projectconfiguration', component:  ProjectconfigurationComponent},
            { path: 'ProjectUserRoleMapping', component:  ProjectUserRoleMappingComponent},
            { path: 'ValidationRule', component:  ValidationRuleComponent},
            { path: 'ClientAdminUserBoard', component:  ClientAdminUserBoardComponent},
            { path: 'ClientAdministration', component:  ClientAdministrationComponent},
            { path: 'UserManagement', component:  UserManagementComponent},
            { path: 'Usergroupmanagement', component:  UsergroupmanagementComponent},
            { path: 'Userrolemanagement', component:  RolemanagementComponent},
            { path: 'RoleDefinition', component:  RoleDefinitionComponent },
            {path: 'workflow', component: WorkflowComponent},
            { path: 'dashboard', component:  DashboardComponent },
            { path: 'dashboard', component:  DashboardComponent },
            { path: 'input', component:  InputComponent }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class LayoutRoutingModule {

}