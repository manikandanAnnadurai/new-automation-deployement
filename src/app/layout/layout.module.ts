import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { HeaderComponent } from './components/header/header.component';
import { SharedModule } from '../shared.module';
import { from } from 'rxjs';
import { ServicecatalogComponent } from './servicecatalog/servicecatalog.component';
import { ValidationRuleComponent } from './validation-rule/validation-rule.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ToastModule } from 'primeng/toast';
import { ClientAdminUserBoardComponent } from './client-admin-user-board/client-admin-user-board.component';
import { ClientAdministrationComponent } from './client-administration/client-administration.component';
import { UserManagementComponent } from './user-management/user-management.component';
import { UsergroupmanagementComponent } from './usergroupmanagement/usergroupmanagement.component';
import { RoleDefinitionComponent } from './role-definition/role-definition.component';
import { ProjectTypeRegulationComponent } from './project-type-regulation/project-type-regulation.component';
import { ProjectdefinitionsComponent } from './projectdefinitions/projectdefinitions.component';
import { ProjectconfigurationComponent } from './projectconfiguration/projectconfiguration.component';
import { ProjectUserRoleMappingComponent } from './project-user-role-mapping/project-user-role-mapping.component';
import { InputSwitchModule } from 'primeng/inputswitch';
import { TooltipModule } from 'primeng/tooltip';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { RolemanagementComponent } from './rolemanagement/rolemanagement.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { WorkflowComponent } from './workflow/workflow.component';
import { InputComponent } from './input/input.component';
import { SearchPipe } from './input/search.pipe';
import { SrchAtrPipe } from './workflow/srch-atr.pipe';
import { SearchOutputAtrPipe } from './workflow/search-output-atr.pipe';
import { AutoCompleteModule } from 'primeng/autocomplete';
//import { UseraccesscontrolComponentsComponent } from './useraccesscontrol-components/useraccesscontrol-components.component';
//import { useraccesscontrolComponent } from './useraccesscontrol/useraccesscontrol.component';
//import { homeComponent } from './home/home.component';
// import { HeaderComponent } from './components/header/header.component';
// import { SidebarComponent } from './components/sidebar/sidebar.component';
// import { FooterComponent } from './components/footer/footer.component';
//import { TrackComponent } from './track/track.component';
// import {ScrollPanelModule} from 'primeng/scrollpanel';

@NgModule({
  imports: [
    CommonModule,
    LayoutRoutingModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    ToastModule,
    TooltipModule,
    InputSwitchModule,
    AngularMultiSelectModule,
    AutoCompleteModule
  ],
  declarations: [
    LayoutComponent,
    ClientAdminUserBoardComponent,
    UsergroupmanagementComponent,
    UserManagementComponent,
    RolemanagementComponent,
    ClientAdministrationComponent,
    SidebarComponent,
    HeaderComponent,
    ServicecatalogComponent,
    ValidationRuleComponent,
    ProjectTypeRegulationComponent,
    ProjectdefinitionsComponent,
    ProjectconfigurationComponent,
    ProjectUserRoleMappingComponent,
    RoleDefinitionComponent,
    DashboardComponent,
    WorkflowComponent,
    InputComponent,
    SearchPipe,
    SrchAtrPipe,
    SearchOutputAtrPipe
  ]
})
export class LayoutModule { }