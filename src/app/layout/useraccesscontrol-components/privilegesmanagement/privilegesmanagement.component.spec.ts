import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrivilegesmanagementComponent } from './privilegesmanagement.component';

describe('PrivilegesmanagementComponent', () => {
  let component: PrivilegesmanagementComponent;
  let fixture: ComponentFixture<PrivilegesmanagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrivilegesmanagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrivilegesmanagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
