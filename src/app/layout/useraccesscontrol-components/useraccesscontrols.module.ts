import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UseraccesscontrolComponentsComponent } from './useraccesscontrol-components.component';
import { UserAccessControlsRoutingModule } from './useraccesscontrols-routing.module';
import { TableModule } from 'primeng/table';
import { ToastModule } from 'primeng/toast';
import { RadioButtonModule } from 'primeng/radiobutton';

import {
  ReactiveFormsModule,
  FormsModule,
  FormGroup,
  FormControl,
  FormArray,
  Validators,
  FormBuilder
} from '@angular/forms';
// import { UserManagementComponent } from './user-management/user-management.component';
import { from } from 'rxjs';
// import { UsergroupmanagementComponent } from './usergroupmanagement/usergroupmanagement.component';
import { RolemanagementComponent } from '../rolemanagement/rolemanagement.component';
import { ModulesmanagementComponent } from './modulesmanagement/modulesmanagement.component';
import { PrivilegesmanagementComponent } from './privilegesmanagement/privilegesmanagement.component';
import { UseractivitylogComponent } from './useractivitylog/useractivitylog.component';
import { SharedModule } from '../../shared.module';
// import { ClientAdministrationComponent } from './client-administration/client-administration.component';
// import { AdminUserBoardComponent } from './admin-user-board/admin-user-board.component';

@NgModule({
  imports: [
    CommonModule,
    TableModule,
    ToastModule,
    RadioButtonModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModule,
    UserAccessControlsRoutingModule
  ],
  
  declarations: [UseraccesscontrolComponentsComponent, RolemanagementComponent, ModulesmanagementComponent, PrivilegesmanagementComponent, UseractivitylogComponent]
})

export class UserAccessControls { }