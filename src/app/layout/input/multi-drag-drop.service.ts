import { Injectable, Input } from '@angular/core';
import { $ } from 'protractor';
import { Observable } from 'rxjs';
import { ifStmt } from '@angular/compiler/src/output/output_ast';

@Injectable({
  providedIn: 'root'
})
export class MultiDragDropService {
  public selectedList = [];
  public currentData: any;
  public draggedFromId: any;
  public hoverListIndex: any;
  public isCopy = false;
  public vCreated = 0;
  public dragData: any = [];
  // public cloner
  public dragcloner: any;

  public virtual = document.createElement('li');

  // tslint:disable-next-line: no-input-rename
  @Input('dragTo') public dragableTo = [];
  constructor() {
    this.virtual.classList.add('virtual');
    this.virtual.setAttribute('id', 'virtual ');
    this.virtual.style.height = '40px';
    this.virtual.style.padding = '10px 15px;';
    this.virtual.style.border = '1px solid #ccc';
    this.virtual.style.background = '#f2f2f2';

  }

  SelectedItem(item, dragData) {
    if (item.selected) {
      item.selected = false;
    } else {
      item.selected = true;
    }
    this.selectedList = [];
    dragData.forEach(elem => {
      if (elem.selected === true) { this.selectedList.push(elem); }
    });



  }
  OnDrag(event, item, dragData, draggedFromId, thiscloner) {
    // event.srcElement.style.display = "none";
    this.dragcloner = thiscloner;
    this.dragData = dragData;
    this.isCopy = event.srcElement.parentElement.getAttribute('data-isCopy') === 'true';
    this.draggedFromId = draggedFromId;
    const k = this.selectedList.findIndex(x => x.id === item.id);
    if (k === -1) { this.selectedList.push(item); } else {
      // this.selectedList.splice(k, 1);
    }




  }
  // data === drop array , dragable To
  OnDrop(event, data, dropToId, dropcloner) {
    // TODO need to drag and drop with partiuclar position for UL
    event.preventDefault();
    if (this.isCopy === true) {

    } else {
      console.log(this.draggedFromId, dropToId);
      if (!dropToId.includes(this.draggedFromId) || (this.dragData === data) ) {
        // same list order
        if (event.target.nodeName.toString() === 'LI') {
          // tslint:disable-next-line: radix
          const b = data.findIndex(x => x.id === parseInt(this.hoverListIndex));
          this.selectedList.forEach((elem, i) => {
            const k = data.findIndex(x => x.id === elem.id);
            //  position to array.splice(movedindex, 1) // splice the moved index

            const tmp = data.splice(k, 1);

            // which position to place them example splice the 5 position to 1
            data.splice(b + i, 0, tmp[0]);
          });
        } else {

        }

        // if end
      } else {
        console.log('else other list drag sd and drop')
        this.selectedList.forEach(elem => {
          // on hover li
          elem.selected = false;
          if (event.target.nodeName.toString() === 'LI') {
            if(data.length > 0){
              if(data.includes(elem)){} else {
                const b = data.findIndex(x => x.id === parseInt(this.hoverListIndex));
                if (b === -1) { data.push(elem); } else {
                  data.splice(b, 0, elem);
                }
              }
              
            }else{
              if(data.includes(elem)){} else {
                data.push(elem);
              }
              
            }
            

          } else {
            if(data.includes(elem)){} else {
              data.push(elem);
            }

          }
        });
        // remove after append from dragged list
        this.selectedList.forEach(elem => {
          const y = this.dragData.findIndex(x => x.id === elem.id);
          if (y === -1) { } else { this.dragData.splice(y, 1); }
          const k = this.dragcloner.findIndex(x => x.id === elem.id);
          if (k === -1) { } else { this.dragcloner.splice(k, 1); }

        });
        // remove after append from dragged list end

        this.dragcloner = [...this.dragData];
        dropcloner = [...data];
        console.log(dropcloner);

      }



      this.selectedList = [];
      this.virtual.remove();

    }




  }




  OnHover(event) {

    try {
      if (event.srcElement.classList.contains('child')) {
        this.hoverListIndex = event.srcElement.parentElement.parentElement.nextElementSibling.dataset.uid;
      } else {

        this.hoverListIndex = event.target.nextElementSibling.dataset.uid;
      }

    } catch (e) {
      this.hoverListIndex = null;
    }
    event.preventDefault();
  }


  OnDragEnter($event) {
    if ($event.srcElement.classList.contains('parent')) {
      $event.srcElement.parentElement.insertBefore(this.virtual, $event.target);
    }
    if ($event.srcElement.classList.contains('draggie')) {

    }
    if ($event.srcElement.classList.contains('child')) {
      try {
        $event.srcElement.parentElement.parentElement.insertBefore(this.virtual, $event.target);
      } catch (e) {

      }
    }
  }
  OnDragLeave($event) {
  }
  OnDragEnd($event) {
    this.virtual.remove();
  }

  filterSearch(fval, fData, searchMap): Observable<any> {

    return fData.filter((obj) => {

      // tslint:disable-next-line: prefer-for-of
      for (let i = 0; i < searchMap.length; i++) {
        if (obj[searchMap[i]].toString().toLowerCase().includes(fval.toLowerCase())) {
          return true;
        }
      }



    });

  }

}
