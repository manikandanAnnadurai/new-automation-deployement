import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../services/common.service';
import { FormBuilder, FormGroup, Validators, FormControl, FormArray } from "@angular/forms";
import { HttpClient } from '@angular/common/http';
import { formValidators } from '../../helper/formValidators';
import { MessageService } from 'primeng/api';
import { ToastModule } from 'primeng/toast';
import jsPDF from 'jspdf';
import 'jspdf-autotable';
import { Router } from "@angular/router";
import { DatePipe } from '@angular/common';
import { ExportService } from 'src/app/services/export.service';
import { ProjectDefinitionService } from '../../services/project-definition.service';
import { ProjectTypeRegulationService } from '../../services/project-type-regulation.service';
import { SharedvalueService } from 'src/app/services/sharedvalue.service';

declare var $: any;
@Component({
  selector: 'app-projectdefinitions',
  templateUrl: './projectdefinitions.component.html',
  providers: [CommonService, MessageService, ToastModule, DatePipe],
  styleUrls: ['./projectdefinitions.component.css']
})


export class ProjectdefinitionsComponent implements OnInit {
  // Project Defintion table variables
  public projectDefinitionList = [];
  public projectDefinitionForm: FormGroup;
  public projectTypeList = [];
  public projectTypeSettings = {};
  public projectStageswithModules = [];
  public modActIndex = 0;
  public isProjectStageActive = 0;
  public isUpdate = false;
  public currentProjectId: number;
  public isSubmitted = false;
  public deleteProjectName: string;
  public exportPdfcolumn = {};
  public exportDataExcelCsv = [];
  public cols: any;
  public stageArr: any;
  public projectList = [];
  public validQuestion: boolean;
  public selectedSubmodule: boolean = false;
  public emptyQuesArr = [];
  public emptyQuesChar = [];
  user_id: number;
  showSideBar = true;
  descriptionResult: boolean;
  // Modal add Edit variable
  constructor(
    private http: HttpClient,
    private formBuilder: FormBuilder,
    private messageService: MessageService,
    private datePipe: DatePipe,
    private router: Router,
    private exporter: ExportService,
    private ProjectDefinition: ProjectDefinitionService,
    private ProjectTypeRegulation: ProjectTypeRegulationService,
    public shrService: SharedvalueService
  ) {
    this.projectTypeSettings = {
      singleSelection: true,
      text: 'Select a Project Type',
      enableSearchFilter: true,
      showCheckbox: false,
      enableFilterSelectAll: false,
    };

  }
  // constructor Ending

  ngOnInit() {
    $('.spinner').show();
    $('.modal').appendTo('#fullscreen');
    this.shrService.getSideBarDetail().subscribe(resp => { this.showSideBar = resp });

    this.projectDefinitionForm = this.formBuilder.group({
      project: ['', [Validators.required, formValidators.alphabetical, formValidators.noWhitespace, Validators.maxLength(50)]],
      projectType: ['', Validators.required],
      description: ['', [Validators.required, formValidators.description, Validators.maxLength(200)]],
      projectIsActive: ['0', Validators.required],
      subModules: this.formBuilder.array([]),
    });

    this.user_id = parseInt(localStorage.getItem('user_id'));

    this.exportPdfcolumn = {
      // columnStyles: { europe: { halign: 'center' } }, // European countries centered
      body: this.projectDefinitionList,
      columns: [
        { header: 'Project', dataKey: 'project' },
        { header: 'Project Type', dataKey: 'projectTypeName' },
        { header: 'Description', dataKey: 'description' },
        { header: 'Created On', dataKey: 'insertedDateTime' },
        { header: 'Status', dataKey: 'projectIsActive' },
      ],
    };


    this.cols = [
      { field: 'project', header: 'Project' },
      { field: 'projectTypeName', header: 'Project Type Name' },
      { field: 'description', header: 'Project Description' },
      { field: 'insertedDateTime', header: 'Created On' },
    ];




    this.GetProjectTypeRegulationList();

    this.GetProjectList();

  };


  isNumber(value: string | number): boolean {
    var result = ((value != null) &&
      (value !== '') &&
      !isNaN(Number(value.toString())));
    this.descriptionResult = result;
    return result;
  }



  onProjectTypeItemSelect(item: any) {
$('.spinner').show();
    const existingItems = this.projectDefinitionForm.get("subModules") as FormArray;
    while (existingItems.length) {
      existingItems.removeAt(0);
    }

    this.isSubmitted = false;

    this.projectStageswithModules = [];
    this.ProjectDefinition.getRequest('GetProjectStageList/' + item.id).subscribe(projectTypeDetailsArray => {
      let projectTypeStageList = projectTypeDetailsArray;
      if (projectTypeStageList.length > 0) {
        let stages = {};
        let projectStages = [];
        projectTypeStageList.forEach(resource => {

          if (!stages.hasOwnProperty(resource.stageId)) {
            stages[resource.stageId] = {};
            stages[resource.stageId]['stageId'] = resource.stageId;
            stages[resource.stageId]['stageName'] = resource.stageName;
            stages[resource.stageId]['isActive'] = 1;
            stages[resource.stageId]['questions'] = [];
            stages[resource.stageId]['modules'] = {};
            JSON.parse(resource.generalQuestions).forEach((elem, index) => {
              stages[resource.stageId]['questions'].push({
                stageid: resource.stageId,
                question: elem['question'],
                answer: ''
              });
            });
          }

          if (!stages[resource.stageId]['modules'].hasOwnProperty(resource.moduleId)) {
            stages[resource.stageId]['modules'][resource.moduleId] = {};
            stages[resource.stageId]['modules'][resource.moduleId]['moduleId'] = resource.moduleId;;
            stages[resource.stageId]['modules'][resource.moduleId]['moduleName'] = resource.moduleName;
            stages[resource.stageId]['modules'][resource.moduleId]['subModules'] = {};
          }


          if (!stages[resource.stageId]['modules'][resource.moduleId]['subModules'].hasOwnProperty(resource.subModuleId)) {
            stages[resource.stageId]['modules'][resource.moduleId]['subModules'][resource.subModuleId] = {};
            stages[resource.stageId]['modules'][resource.moduleId]['subModules'][resource.subModuleId]['subModuleId'] = resource.subModuleId;;
            stages[resource.stageId]['modules'][resource.moduleId]['subModules'][resource.subModuleId]['subModuleName'] = resource.subModuleName;
            stages[resource.stageId]['modules'][resource.moduleId]['subModules'][resource.subModuleId]['isActive'] = 0;
          }

        });
        let stageArray = [];
        const arr = Object.keys(stages).map(i => stages[i]);

        for (var arrayIndex in arr) {
          for (var stageIndex in arr[arrayIndex]) {
            if (stageIndex == 'modules') {
              arr[arrayIndex][stageIndex] = Object.keys(arr[arrayIndex][stageIndex]).map(i => arr[arrayIndex][stageIndex][i]);
              for (var moduleIndex in arr[arrayIndex][stageIndex]) {
                for (var subModuleIndex in arr[arrayIndex][stageIndex][moduleIndex]) {
                  if (subModuleIndex == 'subModules') {
                    arr[arrayIndex][stageIndex][moduleIndex][subModuleIndex] = Object.keys(arr[arrayIndex][stageIndex][moduleIndex][subModuleIndex]).map(i => arr[arrayIndex][stageIndex][moduleIndex][subModuleIndex][i]);
                  }
                }

              }
            }
            stageArray[arr[arrayIndex]['stageId']] = arr[arrayIndex];
          }
        }

        this.stageArr = stageArray;
        projectStages.push({
          projectStages: arr
        });
        this.projectStageswithModules = projectStages;
        //this.modActIndex = this.projectStageswithModules[0].projectStages[0].stageId - 1;
        this.modActIndex = 0;
        this.isProjectStageActive = this.projectStageswithModules[0].projectStages[0].isActive;
        $('#module-main').show();
        this.emptyQuesArr = [];
        for (const key in this.projectStageswithModules[0].projectStages[0].questions) {
          this.emptyQuesArr.push(parseInt(key));
        }
      }
      $('.spinner').hide();
    },err =>{
      $('.spinner').hide();
    });

  }



  GetProjectList() {
    this.ProjectDefinition.getRequest('GetProjectList').subscribe((ProjectDetails) => {
      let projestList = ProjectDetails;
      console.log(ProjectDetails);
      this.projectDefinitionList = [];
      if (projestList.length > 0) {

        var objectsData = ProjectDetails.sort(function (a, b) {
          return a.id - b.id;
        });


        objectsData.forEach(resource => {
          this.projectDefinitionList.push({
            id: resource.projectId,
            project: resource.projectName,
            projectType: { itemName: resource.projectTypeName, id: resource.projectTypeId },
            projectTypeName: resource.projectTypeName,
            description: resource.projectDescription,
            insertedDateTime: this.datePipe.transform(resource.insertedDateTime, 'MM-dd-yyyy'),
            // insertedDateTime: resource.insertedDateTime,
            projectIsActive: resource.active == true ? '1' : '0',
            projectStageId: 1

          });
        });

        console.log(this.projectDefinitionList);

        this.currentProjectId = this.projectDefinitionList[0].id;
      }
    });
  }


  GetProjectTypeRegulationList() {
    $('.spinner').show();
    this.ProjectTypeRegulation.getRequest('GetProjectTypeRegulationList').subscribe((ProjectTypeRegulationDetails) => {
      let projectTypeRegulation = ProjectTypeRegulationDetails;
      if (projectTypeRegulation.length > 0) {
        projectTypeRegulation.forEach((elem, index) => {
          this.projectTypeList.push({ itemName: elem.projectType, id: elem.projectTypeId });
        });
        $('.spinner').hide();
      }else{
        $('.spinner').hide();
      }

    },err =>{
      $('.spinner').hide();
    });
  }



  ClickProject(project_id) {
    localStorage.setItem('project_id', project_id);
    this.router.navigate(['input']);
  }


  // ngOnInit end

  // function start
  editDefinitionModal(project) {
    $('.spinner').show();
    console.log(project);
    this.isUpdate = true;
    this.isSubmitted = false;
    this.ProjectDefinition.getRequest('GetProjectDefinitionListById/' + project.id + '').subscribe(EditProjectDefinitionDetails => {
      let editProjectDetails = EditProjectDefinitionDetails
      console.log(editProjectDetails);
      this.projectDefinitionForm.controls['project'].setValue(project.project);
      this.projectDefinitionForm.controls['projectType'].setValue([project.projectType]);
      this.projectDefinitionForm.controls['description'].setValue(project.description);
      this.projectDefinitionForm.controls['projectIsActive'].setValue(project.projectIsActive);

      const subModuleIds = [];
      editProjectDetails.forEach((elem, index) => {
        $('.' + elem.moduleId + '_' + elem.subModuleId).prop('checked', true);
        let obj = { subModuleId: elem.subModuleId, subModuleName: elem.subModuleName, isActive: 0 };
        this.submoduleOnChange(elem.stageId, obj, 0, elem.moduleId, true);
        subModuleIds.push(elem.subModuleId);
      });
$('.spinner').hide();
      this.OnProjectTypeSelect(project.projectType, subModuleIds, editProjectDetails[0].projectGeneralQA);
    });

    this.currentProjectId = project.id;


  }

  onProjectTypeItemDeSelect(item: any) {
    console.log(item);
  }

  onProjectTypeItemDeSelectAll(items: any) {
    $('#module-main').hide();
    $.each($("input[name='checkbox']:checked"), function () {
      var val = $(this).val();
      $('.' + val).prop("checked", false);
    });
  }

  OnProjectTypeSelect(item: any, subModuleIds, generalQuestionsAns) {
$('.spinner').show();
    this.projectStageswithModules = [];
    this.ProjectDefinition.getRequest('GetProjectStageList/' + item.id).subscribe(projectTypeDetailsArray => {
      let projectTypeStageList = projectTypeDetailsArray;
      if (projectTypeStageList.length > 0) {
        let stages = {};
        let projectStages = [];
        projectTypeStageList.forEach(resource => {

          if (!stages.hasOwnProperty(resource.stageId)) {
            stages[resource.stageId] = {};
            stages[resource.stageId]['stageId'] = resource.stageId;
            stages[resource.stageId]['stageName'] = resource.stageName;
            stages[resource.stageId]['isActive'] = 1;
            stages[resource.stageId]['questions'] = [];
            stages[resource.stageId]['modules'] = {};
            JSON.parse(resource.generalQuestions).forEach((elem, index) => {
              stages[resource.stageId]['questions'].push({
                stageid: resource.stageId,
                question: elem['question'],
                answer: ''
              });
            });
          }

          if (!stages[resource.stageId]['modules'].hasOwnProperty(resource.moduleId)) {
            stages[resource.stageId]['modules'][resource.moduleId] = {};
            stages[resource.stageId]['modules'][resource.moduleId]['moduleId'] = resource.moduleId;;
            stages[resource.stageId]['modules'][resource.moduleId]['moduleName'] = resource.moduleName;
            stages[resource.stageId]['modules'][resource.moduleId]['subModules'] = {};
          }


          if (!stages[resource.stageId]['modules'][resource.moduleId]['subModules'].hasOwnProperty(resource.subModuleId)) {
            stages[resource.stageId]['modules'][resource.moduleId]['subModules'][resource.subModuleId] = {};
            stages[resource.stageId]['modules'][resource.moduleId]['subModules'][resource.subModuleId]['subModuleId'] = resource.subModuleId;
            stages[resource.stageId]['modules'][resource.moduleId]['subModules'][resource.subModuleId]['subModuleName'] = resource.subModuleName;
            stages[resource.stageId]['modules'][resource.moduleId]['subModules'][resource.subModuleId]['isActive'] = subModuleIds.includes(resource.subModuleId) ? 1 : 0;
          }

        });
        let stageArray = [];
        const arr = Object.keys(stages).map(i => stages[i]);

        for (var arrayIndex in arr) {
          for (var stageIndex in arr[arrayIndex]) {
            if (stageIndex == 'modules') {
              arr[arrayIndex][stageIndex] = Object.keys(arr[arrayIndex][stageIndex]).map(i => arr[arrayIndex][stageIndex][i]);
              for (var moduleIndex in arr[arrayIndex][stageIndex]) {
                for (var subModuleIndex in arr[arrayIndex][stageIndex][moduleIndex]) {
                  if (subModuleIndex == 'subModules') {
                    arr[arrayIndex][stageIndex][moduleIndex][subModuleIndex] = Object.keys(arr[arrayIndex][stageIndex][moduleIndex][subModuleIndex]).map(i => arr[arrayIndex][stageIndex][moduleIndex][subModuleIndex][i]);
                  }
                }

              }
            }
            stageArray[arr[arrayIndex]['stageId']] = arr[arrayIndex];
          }
        }

        this.stageArr = stageArray;
        projectStages.push({
          projectStages: arr
        });
        this.projectStageswithModules = projectStages;
        //this.modActIndex = this.projectStageswithModules[0].projectStages[0].stageId - 1;
        this.modActIndex = 0;
        this.isProjectStageActive = this.projectStageswithModules[0].projectStages[0].isActive;
        $('#module-main').show();
        this.projectStageswithModules[0].projectStages[0].questions = JSON.parse(generalQuestionsAns);

        this.emptyQuesArr = [];
        for (const key in this.projectStageswithModules[0].projectStages[0].questions) {
          if (this.projectStageswithModules[0].projectStages[0].questions[key]['answer'] == '') {
            this.emptyQuesArr.push(parseInt(key));
          }
        }

      }
    });
    $('.spinner').hide();
  }

  deleteProject(project) {
    this.currentProjectId = project.id;
    this.deleteProjectName = project.project;
  }
  deleteProjectConfirm() {

    this.ProjectDefinition.getRequest('DeleteProjectDefinition/' + this.currentProjectId).subscribe((response) => {

      if (response.status == 1) {
        this.GetProjectList();
        $('.modal-close').trigger('click');
        this.successmessage("project definition deleted successfully");
      }
      else {
        if (response["status"] == 2) {
          this.Errormessage('Project definition already exists');
        } else {
          this.Errormessage(response.message);
        }
      }
    });

  }


  setModuleActiveIndex(stage) {
    this.modActIndex = stage.stageId;
    this.isProjectStageActive = stage.isActive;
  }

  Close() {
    this.projectDefinitionForm.reset();
    this.projectStageswithModules = [];
    this.isSubmitted = false;
    $('#module-main').hide();
    $.each($("input[name='checkbox']:checked"), function () {
      var val = $(this).val();
      $('.' + val).prop("checked", false);
    });
  }

  submoduleOnChange(stageId, submodule, moduleIndex: number, moduleId: number, isChecked: boolean) {

    const subModulesArray = <FormArray>this.projectDefinitionForm.controls.subModules;
    if (isChecked) {
      $('.' + moduleId + '_' + submodule.subModuleId).prop('checked', true);
      subModulesArray.push(new FormControl(stageId + '_' + moduleId + '_' + submodule.subModuleId));
    } else {
      $('.' + moduleId + '_' + submodule.subModuleId).prop('checked', false);
      let index = subModulesArray.controls.findIndex(x => x.value == stageId + '_' + moduleId + '_' + submodule.subModuleId)
      subModulesArray.removeAt(index);
    }


    if (subModulesArray.length > 0) {
      this.selectedSubmodule = true;
    } else {
      this.selectedSubmodule = false;
    }


    console.log(this.selectedSubmodule);

  }


  onSearchChange(value, aryValue) {
    var res = value.match(/^[a-zA-Z0-9]*$/);
    if (value != '') {
      this.emptyQuesArr = this.emptyQuesArr.filter(x => x !== aryValue);
      if (res == null) {
        $('#question_' + aryValue).show();
        this.emptyQuesChar.push(aryValue);
      } else {
        $('#question_' + aryValue).hide();
        this.emptyQuesChar = this.emptyQuesChar.filter(x => x !== aryValue);
      }

    } else {
      this.emptyQuesArr.push(aryValue);
      $('#question_' + aryValue).hide();
      this.emptyQuesChar = this.emptyQuesChar.filter(x => x !== aryValue);
    }
  }



  addOpenModel() {
    this.isUpdate = false;
  }


  submitForm(formData) {
$('.spinner').show();
    this.isSubmitted = true;
    this.selectedSubmodule == true ? true : false;
    this.isNumber(formData.description);

    var filteredSubModules = formData.subModules.filter(function (el) {
      return el != null;
    });

    if (filteredSubModules.length == 0) {
      this.selectedSubmodule = false;
    }

    if (filteredSubModules.length > 0 && this.projectDefinitionForm.valid && this.descriptionResult == false) {
      this.selectedSubmodule = true;
      let projectDefinitionObj = {};
      let projectDefinitionArr = [];

      if (this.emptyQuesArr.length == 0 && this.emptyQuesChar.length == 0) {

        if (!this.isUpdate) {

          filteredSubModules.forEach((elem, index) => {
            let element = elem.split('_');
            projectDefinitionArr.push({
              ProjectName: formData.project,
              ProjectDescription: formData.description,
              ProjectTypeId: formData.projectType[0].id,
              UserId: this.user_id,
              StageId: parseInt(element[0]),
              SubModuleId: parseInt(element[2]),
              DisplayOrder: 1,
              Active: formData.projectIsActive == "1" ? true : false,
              Flag: 0,
              Questions: JSON.stringify(this.stageArr[parseInt(element[0])].questions)
            });

          });

          this.ProjectDefinition.postRequest('InsertProjectDefinition', projectDefinitionArr).subscribe((response) => {
            if (response.status == 1) {
              $('.close').trigger('click');
              this.Close();
              this.GetProjectList();
              this.successmessage("Project definition created successfully");
              this.isSubmitted = false;
              $('.spinner').hide();
            }
            else {
              if (response.status == 2) {
                $('.spinner').hide();
                this.Errormessage(response.message);
              } else {
                $('.spinner').hide();
                this.Errormessage(response.message);
              }
            }
          },err =>{
            $('.spinner').hide();
                this.Errormessage(err.message);
          });

        } else {

          filteredSubModules.forEach((elem, index) => {
            let element = elem.split('_');
            console.log(element);
            projectDefinitionArr.push({
              ProjectId: this.currentProjectId,
              ProjectName: formData.project,
              ProjectDescription: formData.description,
              ProjectTypeId: formData.projectType[0].id,
              UserId: this.user_id,
              StageId: parseInt(element[0]),
              SubModuleId: parseInt(element[2]),
              DisplayOrder: 1,
              Active: formData.projectIsActive == "1" ? true : false,
              Flag: 1,
              Questions: JSON.stringify(this.stageArr[parseInt(element[0])]['questions'])
            });
          });


          this.ProjectDefinition.postRequest('UpdateProjectDefinition', projectDefinitionArr).subscribe((response) => {
            if (response.status == 1) {
              $('.close').trigger('click');
              this.Close();
              $('.spinner').hide();
              this.GetProjectList();
              this.successmessage("Project definition updated successfully");
              this.isSubmitted = false;
              
            }
            else {
              if (response.status == 2) {
                this.Errormessage(response.message);
                $('.spinner').hide();
              } else {
                $('.spinner').hide();
                this.Errormessage(response.message);
              }
            }
          },err =>{
            $('.spinner').hide();
          });

        }

      } else {
        $('.spinner').hide();
        this.validQuestion = false;
      }

    }

  }

  exportPdf() {
    this.exporter.exportPdf(this.exportPdfcolumn, 'project_definition');
  }
  exportCSV() {
    this.exportDataExcelCsv = [];
    this.projectDefinitionList.forEach((elem, i) => {
      let obj = {
        'Project': elem.project,
        'Project Type': elem.projectTypeName,
        'Project Description': elem.description,
        'Created On': elem.insertedDateTime,
        'Status': elem.projectIsActive
      }
      this.exportDataExcelCsv.push(obj);
    });

    this.exporter.exportCSV(this.exportDataExcelCsv, 'project_definition');
  }
  exportExcel() {
    this.exportDataExcelCsv = [];
    this.projectDefinitionList.forEach((elem, i) => {
      let obj = {
        'Project': elem.project,
        'Project Type': elem.projectTypeName,
        'Project Description': elem.description,
        'Created On': elem.insertedDateTime,
        'Status': elem.projectIsActive
      }
      this.exportDataExcelCsv.push(obj);
    });
    this.exporter.exportExcel(this.exportDataExcelCsv, 'project_definition');
  }



  successmessage(message) {
    this.messageService.add({ severity: 'success', summary: 'Success', detail: message });
  }

  Errormessage(errorsmessage) {
    this.messageService.add({ severity: 'error', summary: 'Error', detail: errorsmessage });
  }

}
