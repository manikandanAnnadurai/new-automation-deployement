import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../services/common.service';
import { MessageService } from 'primeng/api';
import { ToastModule } from 'primeng/toast';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup, Validators, FormControl } from "@angular/forms";
import { formValidators } from "../../helper/formValidators";
import { NgForm } from '@angular/forms';
import { ClientAdministrationService } from '../../services/client-administration.service';
import { SharedvalueService } from 'src/app/services/sharedvalue.service';

declare var $: any
@Component({
  selector: 'app-client-administration',
  templateUrl: './client-administration.component.html',
  providers: [CommonService, MessageService, ToastModule],
  styleUrls: ['./client-administration.component.css']
})

export class ClientAdministrationComponent implements OnInit {

  selectedValue: any;
  cols: any[];
  clientAdministration: any;
  clientAdministrationForm: FormGroup;
  showCount: boolean;
  disabled: boolean;
  uploadData: any;
  webUrl: string;
  organizationName: string;
  pointOfContact: string;
  phoneNumber: string;
  firstName: string;
  lastName: string;
  address: string;
  password: string;
  emailId: string;
  userId: number;
  userLoginId: number;
  showSideBar = true;
  isSubmitted = false;
  PasswordErrorMessage: string;
  update: boolean;
  addressResult: boolean;



  constructor(
    private http: HttpClient,
    private formBuilder: FormBuilder,
    private ClientAdministration: ClientAdministrationService,
    private messageService: MessageService,
    public shrService: SharedvalueService
  ) { }

  ngOnInit() {
    $('.spinner').show();
    this.shrService.getSideBarDetail().subscribe(resp => { this.showSideBar = resp });
    this.selectedValue = 'true';
    this.GetClientAdministrationList();
    this.clientAdministrationForm = this.formBuilder.group({
      OrganizationName: ["", [Validators.required, formValidators.alphabetical, formValidators.noWhitespace, Validators.maxLength(50)]],
      WebUrl: ["", [Validators.required, formValidators.url]],
      Address: ["", [Validators.required, formValidators.address]],
      FirstName: ["", [Validators.required, formValidators.alphabetical, formValidators.noWhitespace, Validators.maxLength(50)]],
      LastName: ["", [Validators.required, formValidators.alphabetical, formValidators.noWhitespace, Validators.maxLength(50)]],
      Email: ["", [Validators.required, formValidators.email, Validators.maxLength(50)]],
      Password: ["", [Validators.required, Validators.minLength(8), Validators.maxLength(12)]],
      PhoneNumber: ["", [Validators.required, formValidators.noWhitespace, formValidators.numeric, Validators.minLength(7), Validators.maxLength(15)]],
      active: "",
      rememberMeFlag: [false]
    });

    this.cols = [
      { field: 'organizationName', header: 'Organization Name' },
      { field: 'emailId', header: 'Email' },
      { field: 'phoneNumber', header: 'Phone' },
      { field: 'webUrl', header: 'Web Url' }

    ];
  }


  GetClientAdministrationList() {
    this.ClientAdministration.getRequest('GetClientDetails').subscribe((ClientAdministrationDetails) => {
      this.clientAdministration = ClientAdministrationDetails;
      this.showCount = true;
      $('.spinner').hide();
      console.log(ClientAdministrationDetails);
    },err =>{
      $('.spinner').hide();
    });
  }


  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }


  public get getFields() {
    return this.clientAdministrationForm.controls;
  }


  ValidatePassword(password: string) {
    var input = password;
    var ErrorMessage = '';
    if (input == null || input == '') {
      this.PasswordErrorMessage = "Password should not be empty";
    }
    var hasNumber = new RegExp("[0-9]+");
    var hasUpperChar = new RegExp("[A-Z]+");
    var hasMiniMaxChars = new RegExp("^.{8,15}$");
    var hasLowerChar = new RegExp("[a-z]+");
    var hasSymbols = new RegExp("[!@#$%^&*(),.?/:{}|<>]");

    if (!hasUpperChar.test(input)) {
      this.PasswordErrorMessage = "Password should contain at least one upper case letter.";
      return this.PasswordErrorMessage;
    }
    else if (!hasLowerChar.test(input)) {
      this.PasswordErrorMessage = "Password should contain at least one lower case letter.";
      return this.PasswordErrorMessage;
    }
    else if (!hasMiniMaxChars.test(input)) {
      this.PasswordErrorMessage = "Password should be min 8 to 15 characters.";
      return this.PasswordErrorMessage;
    }
    else if (!hasNumber.test(input)) {
      this.PasswordErrorMessage = "Password should contain at least one numeric value.";
      return this.PasswordErrorMessage;
    } else if (!hasSymbols.test(input)) {
      this.PasswordErrorMessage = "Password should contain at least one special case character.";
      return this.PasswordErrorMessage;
    }
    else {
      return this.PasswordErrorMessage = 'true';
    }
  }




  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  submitForm(params) {
  
    console.log(params);
    console.log(this.clientAdministrationForm.valid);
    let formData = params.value;
    this.isSubmitted = true;
    this.validateAllFormFields(this.clientAdministrationForm);
    this.ValidatePassword(formData["Password"]);
    this.isNumber(formData["Address"]);

    var data = {};
    if (this.clientAdministrationForm.valid && this.update != true && this.PasswordErrorMessage == 'true' && this.addressResult == false) {
      $('.spinner').show();
      this.disabled = true;

      data = {
        OrganizationName: formData["OrganizationName"],
        WebUrl: formData["WebUrl"],
        Address: formData["Address"],
        FirstName: formData["FirstName"],
        LastName: formData["LastName"],
        PhoneNumber: formData["PhoneNumber"],
        EmailId: formData["Email"],
        Password: formData["Password"],
        Active: (formData["active"] == "true"),
        RecordStatus: 1,
        InsertedBy: 1,
      }

      this.ClientAdministration.postRequest('InsertClientDetails', data).subscribe((response) => {
      
        if (response.status == 1) {
          $('.close').trigger('click');
          this.Close();
          this.GetClientAdministrationList();
          this.successmessage("Client created successfully");
          this.disabled = false;
          this.isSubmitted = false;
        }
        else {
          $('.spinner').hide();
          this.Errormessage(response["message"]);
          this.disabled = false;
        }
      },err =>{
        this.Errormessage(err.message);
        $('.spinner').hide();
      });
    } else {
      if (this.clientAdministrationForm.valid && this.PasswordErrorMessage == 'true' && this.update == true) {
        $('.spinner').show();
        this.disabled = true;
        data = {
          ClientId: this.uploadData["clientId"],
          UserId: this.uploadData["userId"],
          UserloginId: this.uploadData["userLoginId"],
          OrganizationName: formData["OrganizationName"],
          WebUrl: formData["WebUrl"],
          Address: formData["Address"],
          FirstName: formData["FirstName"],
          LastName: formData["LastName"],
          PhoneNumber: formData["PhoneNumber"],
          EmailId: formData["Email"],
          Password: formData["Password"],
          Active: (formData["active"] == "true"),
          RecordStatus: 1,
          InsertedBy: 1,
          UpdatedBy: 1,
          InsertedDateTime: this.uploadData["insertedDateTime"]
        }



        this.ClientAdministration.postRequest('UpdateClientDetails', data).subscribe((response) => {
          if (response.status == 1) {
            $('.close').trigger('click');
            this.Close();
            this.GetClientAdministrationList();
            this.successmessage("Client updated Successfully");
            this.disabled = false;
          } else if (response.status == 3) {
            $('.spinner').hide();
            this.Errormessage(response["message"]);
            this.disabled = false;
          } else { $('.spinner').hide();
            this.Errormessage(response["message"]);
            this.disabled = false;
          }
        },err => {
          $('.spinner').hide();
        });
      }
    }
  }

  isNumber(value: string | number): boolean {
    var result = ((value != null) &&
      (value !== '') &&
      !isNaN(Number(value.toString())));
    this.addressResult = result;
    return result;
  }


  editClientAdministration(clientAdministration: any) {
console.log(clientAdministration);
    this.update = true;
    this.clientAdministrationForm.controls['OrganizationName'].setValue(clientAdministration.organizationName);
    this.clientAdministrationForm.controls['WebUrl'].setValue(clientAdministration.webUrl);
    this.clientAdministrationForm.controls['Address'].setValue(clientAdministration.address);
    this.clientAdministrationForm.controls['FirstName'].setValue(clientAdministration.firstName);
    this.clientAdministrationForm.controls['LastName'].setValue(clientAdministration.lastName);
    this.clientAdministrationForm.controls['PhoneNumber'].setValue(clientAdministration.phoneNumber);
    this.clientAdministrationForm.controls['Email'].setValue(clientAdministration.emailId);
    this.clientAdministrationForm.controls['Password'].setValue(clientAdministration.password);
    this.selectedValue = String(clientAdministration.active);
    this.clientAdministrationForm.controls['active'].setValue(this.selectedValue);
    this.uploadData = clientAdministration;
  }


  Open() {
    this.selectedValue = 'true';
    this.isSubmitted = false;
    this.update = false;
  }

  Close() {
    this.clientAdministrationForm.reset();
    this.isSubmitted = false;
    this.update = false;
  }

  successmessage(message) {
    this.messageService.add({ severity: 'success', summary: 'Success', detail: message });
  }

  Errormessage(errorsmessage) {
    this.messageService.add({ severity: 'error', summary: 'Error', detail: errorsmessage });
  }


  deleteClientDetails(clientAdministration: any) {
$('.spinner').show();
    var data = {
      ClientId: clientAdministration['clientId'],
      UserId: clientAdministration['userId'],
      UserLoginId: clientAdministration['userLoginId']
    }

    this.ClientAdministration.postRequest('DeleteClientDetails', data).subscribe((response) => {
      if (response.status == 1) {
        this.GetClientAdministrationList();
        this.successmessage("Clients Details deleted Successfully");
      }
      else {
        this.Errormessage(response["message"]);
      }
    });
  }

}
