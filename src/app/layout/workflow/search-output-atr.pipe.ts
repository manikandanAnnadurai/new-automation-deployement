import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'searchOutputAtr',
  pure: false

})
export class SearchOutputAtrPipe implements PipeTransform {

  transform(items: any[], filterdata: string): any[] {
    if (!items) { return []; }
    if (!filterdata) { return items; }
    filterdata = filterdata.toString().toLowerCase();
    return items.filter(it => {
      return it.outputpropertyname.toLowerCase().includes(filterdata);
    });
  }

}
