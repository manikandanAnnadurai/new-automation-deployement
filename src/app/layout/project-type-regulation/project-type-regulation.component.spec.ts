import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectTypeRegulationComponent } from './project-type-regulation.component';

describe('ProjectTypeRegulationComponent', () => {
  let component: ProjectTypeRegulationComponent;
  let fixture: ComponentFixture<ProjectTypeRegulationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectTypeRegulationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectTypeRegulationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
