import { Component, OnInit } from '@angular/core';
import { isBuffer } from 'util';
import { MessageService } from 'primeng/api';
import { ToastModule } from 'primeng/toast';
import { CommonService } from '../../services/common.service';
import jsPDF from 'jspdf';
import 'jspdf-autotable';
declare var $: any;
@Component({
  selector: 'app-rolemanagement',
  templateUrl: './rolemanagement.component.html',
  styleUrls: ['./rolemanagement.component.css'],
  providers: [CommonService, MessageService, ToastModule],
})
export class RolemanagementComponent implements OnInit {
  public userRoles: any;
  public users: any;
  public projects: any;
  public pmList = [];
  public tlList = [];
  public qcList = [];
  public puList = [];
  public pmListLocal = [];
  public tlListLocal = [];
  public qcListLocal = [];
  public puListLocal = [];
  public activeTab = 'pm';
  public selectedTableUser = [];
  public savebuttonDisable = true;
  public cols: any;
  public exportPdfcolumn: any;
  public tabId: any;
  public mtabId: any;
  public roles = [];
  public cloneOfUsers = [];
  dummyRows: any;
  selectedUserwithRole = [];
  public previousTab: any;
  public tabIdRole: string;
  public mtabIdRole: string;
  public tableRoleUserList = [];
  public selectedTableUserWRole = [];

  constructor(private messageService: MessageService) { }

  ngOnInit() {
  }

  successmessage(message) {
    this.messageService.add({ severity: 'success', summary: 'Success', detail: message });
  }

  Errormessage(errorsmessage) {
    this.messageService.add({ severity: 'error', summary: 'Error', detail: errorsmessage });
  }
  clear() {
    this.messageService.clear();
  }

  tabSelect(role) {
    this.tabId = role.id;
    this.tabIdRole = role.roleName;
  }
  setactiveTabInModal(role, index) {
    this.previousTab = this.mtabId;
    this.mtabId = role.id;
    this.mtabIdRole = role.roleName;



    this.selectedTableUser = [];
    const idx = this.selectedUserwithRole.findIndex(x => x.roleId === this.mtabId);
    if (idx === -1) { this.selectedTableUser = []; } else {
      console.log('push happen');

      this.selectedUserwithRole[idx].userList.forEach((elem, i) => {

        this.selectedTableUser[i] = { name: elem.userName, id: elem.id , mailId: elem.mailId };
      })
      console.log(this.selectedTableUser);
    }





  }

  onRowSelect($event) {
    console.log($event);
    // this.selectedUserwithRole = [{
    //   roleId: 11,
    //   roleName: 'Production User',
    //   userList: [{ name: 'Naveeen', id : 1 }]
    // },
    // {
    //   roleId: 12,
    //   roleName: 'Qc Lead',
    //   userList: [{ name: 'Priya', id: 2 }]
    // },
    // ];
    let obj;
    let index;
    if (this.selectedUserwithRole.length) {
      index = this.selectedUserwithRole.findIndex(x => x.roleId === this.mtabId);
    } else {
      index = -1;
    }

    if (index === -1) {


      obj = {
        roleId: this.mtabId,
        roleName: this.mtabIdRole,
        userList: [{ userName: $event.data.name, id: $event.data.id, mailId: $event.data.mailId }]
      };
      this.selectedUserwithRole.push(obj);
    } else {
      obj = { userName: $event.data.name, id: $event.data.id, mailId: $event.data.mailId  };
      const sno = this.selectedUserwithRole[index].userList.findIndex(x => x.id === $event.data.id);
      if (sno === -1) { this.selectedUserwithRole[index].userList.push(obj) }

    }

    console.log(this.selectedTableUser);

  }

  onRowUnselect($event) {

    console.log($event);

    const index = this.selectedUserwithRole.findIndex(x => x.roleId === this.mtabId);

    const i = this.selectedUserwithRole[index].userList.findIndex(x => x.id === $event.data.id);
    this.selectedUserwithRole[index].userList.splice(i, 1);

  }
  headerCheckboxToggle($event) {
    console.log($event);
    let obj;

   
    if ($event.checked) {
    
      this.users.forEach((elem, i) => {
        const k = this.selectedUserwithRole.findIndex(x => x.roleId === this.mtabId);
        obj = {
          roleId: this.mtabId,
          roleName: this.mtabIdRole,
          userList: [{ userName: elem.name, id: elem.id, mailId: elem.mailId }]
        };

        if (k === -1) {
          this.selectedUserwithRole.push(obj);
        } else {
          const a = this.selectedUserwithRole[k].userList.findIndex(x => x.id === elem.id);
          if(a === -1 ) {this.selectedUserwithRole[k].userList.push({ userName: elem.name, id: elem.id, mailId: elem.mailId });}
        }

      });

    } else {
      const k = this.selectedUserwithRole.findIndex(x => x.roleId === this.mtabId);
      if (k === -1) { } else {
        this.selectedUserwithRole.splice(k, 1);
      }
    }
  }

  delUserFromGroup(item: any, user) {
    // current table values only get {name ,user id}
    console.log(this.selectedTableUser);

    // role Id , user Id
    console.log(item, user);
    const i = this.selectedUserwithRole.findIndex(x => x.roleId === item.roleId);
    if (this.selectedUserwithRole[i].userList === []) { } else {
      const j = this.selectedUserwithRole[i].userList.findIndex(x => x.id === user.id);

      this.selectedUserwithRole[i].userList.splice(j, 1);

    }
    if (this.mtabId === item.roleId) {
      const k = this.selectedTableUser.findIndex(x => x.id === user.id);
      this.selectedTableUser.splice(k, 1);

      const clone = [...this.selectedTableUser];
      this.selectedTableUser = [];
      this.selectedTableUser = clone;
    }

  }

  addUserInLocal() {
    console.log(this.selectedTableUser);
    this.pmListLocal = [...this.pmList];
    this.qcListLocal = [...this.qcList];
    this.tlListLocal = [...this.tlList];
    this.puListLocal = [...this.puList];
    this.savebuttonDisable = false;
    switch (this.activeTab) {

      case 'pm':


        this.selectedTableUser.forEach(elem => {
          this.pmListLocal.push(elem);

        });
        // tslint:disable-next-line: align
        this.pmListLocal = this.selectedTableUser.filter(value => -1 !== this.pmListLocal.indexOf(value));
        break;
      case 'qc':

        this.selectedTableUser.forEach(elem => {
          this.qcListLocal.push(elem);
        });
        // tslint:disable-next-line: align
        this.qcListLocal = this.selectedTableUser.filter(value => -1 !== this.qcListLocal.indexOf(value));

        break;
      case 'tl':

        this.selectedTableUser.forEach(elem => {
          this.tlListLocal.push(elem);
        });
        // tslint:disable-next-line: align
        this.tlListLocal = this.selectedTableUser.filter(value => -1 !== this.tlListLocal.indexOf(value));

        break;
      case 'pu':

        this.selectedTableUser.forEach(elem => {
          this.puListLocal.push(elem);
        });
        // tslint:disable-next-line: align
        this.puListLocal = this.selectedTableUser.filter(value => -1 !== this.puListLocal.indexOf(value));

        break;
    }
  }

  updateUser() {
    console.log(this.selectedUserwithRole);

    let obj;
    this.selectedUserwithRole.forEach((elem, i) => {

      const a = this.selectedUserwithRole.findIndex( x => x.roleId === elem.roleId);
      this.tableRoleUserList[elem.roleId].data = [];
      console.log('this.selectedUserwithRole[a]');
      console.log(this.selectedUserwithRole[a]);
      this.selectedUserwithRole[a].userList.forEach((val, y) => {
        if (a === -1) {
          obj = {
            roleId: elem.roleId,
            data : [{ name : val.userName, id: val.id, mailId: val.mailId, createdBy: 'Admin', insertedDateTime: new Date() }]
          };
          this.tableRoleUserList[elem.roleId] = obj;
        } else {
         const b = this.tableRoleUserList[elem.roleId].data.findIndex( x => x.id === val.id);
         if (b === -1) {
          obj = { name : val.userName, id: val.id, mailId: val.mailId, createdBy: 'Admin', insertedDateTime: new Date() };
          this.tableRoleUserList[elem.roleId].data.push(obj);
         }else{

         }

        }
      });
     
    });
   console.log(this.tableRoleUserList);
    $('.close-modal').trigger('click');
    this.successmessage('User added Successfully');

  }

  openModal(){
    
    this.mtabId = this.roles[0].id;
    this.selectedTableUser = [];
    if(this.tableRoleUserList[this.mtabId].data.length){
      console.log('inside');
      this.tableRoleUserList[this.mtabId].data.forEach((elem, i) => {
       let obj = {name: elem.name, id: elem.id, mailId: elem.mailId};
       this.selectedTableUser.push(obj);
      });
      let cloner = [...this.selectedTableUser];
      this.selectedTableUser = [];
      this.selectedTableUser = cloner;
    }


   



    console.log(this.tableRoleUserList);
    console.log(this.selectedTableUser);


  }


  delUserFromProject(user) {
    console.log(user);
    const i = this.tableRoleUserList[this.tabId].data.findIndex(x=> x.id === user.id);
    this.tableRoleUserList[this.tabId].data.splice(i, 1);
    console.log(this.selectedUserwithRole);
    const j = this.selectedUserwithRole.findIndex(x=> x.roleId === this.tabId);
    const k =  this.selectedUserwithRole[j].userList.findIndex(x=> x.id === user.id);
    console.log(k);
    this.selectedUserwithRole[j].userList.splice(k, 1);
    console.log(this.selectedUserwithRole);
   // this.successmessage('User deleted successfully');
  }
  delMultiUserFromProject(tab) {

    this.selectedTableUserWRole.forEach((elem, i) => {
      const index = this.tableRoleUserList[this.tabId].data.findIndex(x => x.id === elem.id);
      if (index === -1) { } else { this.tableRoleUserList[this.tabId].data.splice(index, 1); }

      const j = this.selectedUserwithRole.findIndex(x=> x.roleId === this.tabId);
      if(j === -1){} else {
       const k = this.selectedUserwithRole[j].userList.findIndex(x=> x.id === elem.id);
       if(k === -1){} else{
        this.selectedUserwithRole[j].userList.splice(k, 1);
       }
      

      }
    });

    if(this.tableRoleUserList[this.tabId].data.length){
     
    }


    this.successmessage('Users deleted successfully');
  }

  delUserFromSelectionModal(user) {
    this.dummyRows = [...this.selectedTableUser];
    this.dummyRows.forEach((elem, i) => {
      if (user.id === elem.id) {
        this.dummyRows.splice(i, 1);

      }
    });
    this.selectedTableUser = this.dummyRows;
  }

  exportPdf(tab) {
    let data;
    switch (tab) {
      case 'pm':
        data = this.pmList;
        break;
      case 'qc':
        data = this.qcList;
        break;
      case 'tl':
        data = this.tlList;
        break;
      case 'pu':
        data = this.puList;
        break;

    }
    this.cols = [
      { field: 'name', header: 'Group Name' },
      { field: 'mailId', header: 'No of Users' },
      { field: 'createdBy', header: 'Created by' },
      { field: 'insertedDateTime', header: 'Created on' },

    ];

    this.exportPdfcolumn = this.cols.map(col => ({ title: col.header, dataKey: col.field }));
    const doc = new jsPDF('l');
    doc.autoTable(this.exportPdfcolumn, data);
    doc.save('UDP_' + tab + '_' + new Date().getTime() + '.pdf');

  }


  exportExcel(tab) {
    let data;
    switch (tab) {
      case 'pm':
        data = this.pmList;
        break;
      case 'qc':
        data = this.qcList;
        break;
      case 'tl':
        data = this.tlList;
        break;
      case 'pu':
        data = this.puList;
        break;

    }
    import('xlsx').then(xlsx => {
      const worksheet = xlsx.utils.json_to_sheet(data);
      const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
      this.saveAsExcelFile(excelBuffer, 'UDP_' + tab);
    });
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    import('file-saver').then(FileSaver => {


      let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      let EXTENSION_TYPE = '.xlsx';
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(data, fileName + '_' + new Date().getTime() + EXTENSION_TYPE);
    });
  }


  exportCSV(tab) {
    let data;
    switch (tab) {
      case 'pm':
        data = this.pmList;
        break;
      case 'qc':
        data = this.qcList;
        break;
      case 'tl':
        data = this.tlList;
        break;
      case 'pu':
        data = this.puList;
        break;

    }
    console.log(data);
    import('xlsx').then(xlsx => {
      const worksheet = xlsx.utils.json_to_sheet(data);
      const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = xlsx.write(workbook, { bookType: 'csv', type: 'array' });

      this.saveAsCSVFile(excelBuffer, 'UDP_' + tab);
    });
  }

  saveAsCSVFile(buffer: any, fileName: string): void {
    import('file-saver').then(FileSaver => {


      let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      let EXTENSION_TYPE = '.csv';
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(data, fileName + '_' + new Date().getTime() + EXTENSION_TYPE);
    });
  }

}
