import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ToastModule } from 'primeng/toast';
//import {TableModule} from 'primeng/table';
//import {ChartModule, TreeTableModule,  DropdownModule, MultiSelectModule, CarouselModule, TooltipModule} from 'primeng/primeng';
import {
    ReactiveFormsModule,
    FormsModule,
    FormGroup,
    FormControl,
    Validators,
    FormBuilder
  } from '@angular/forms';
  import { SharedModule } from '../shared.module';
import { TrackComponent } from './track.component';
import { TrackRoutingModule } from './track-routing.module';


  @NgModule({
    imports: [
      CommonModule,
      ToastModule,
      TrackRoutingModule, FormsModule, ReactiveFormsModule,
      SharedModule,
    ],
    declarations: [TrackComponent]
  })
  export class TrackModule { }